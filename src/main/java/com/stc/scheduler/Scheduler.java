package com.stc.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 05/01/2021
 * Time      : 09:06
 * Filename  : Scheduler
 */
@Slf4j
@Component
public class Scheduler {

    /***
     * fixedDelay: thời gian lặp lại, đơn vị millisecond,
     * task trước chạy xong thì mới lặp lại lần chạy tiếp theo
     * @throws InterruptedException
     */
    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() throws InterruptedException {
        log.info("Task1 - " + new Date());
    }

    /***
     * fixedRate: thời gian lặp lại, đơn vị millisecond,
     * cứ sau khoảng thời gian thì lặp lại, không quan tâm lần chạy trước đó
     * @throws InterruptedException
     */
    @Scheduled(fixedRate = 1000)
    public void scheduleFixedRateTask() throws InterruptedException {
        log.info("Task2 - " + new Date());
    }

    /***
     * cron: biểu thức định nghĩa thời gian lặp lại
     * generate tại đây: https://www.freeformatter.com/cron-expression-generator-quartz.html
     * @throws InterruptedException
     */
    @Scheduled(cron = " 5 * * * * ?")
    public void scheduleTaskUsingCronExpression() throws InterruptedException {
        log.info("Task3 - " + new Date());
    }
}
